package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	
	@Autowired
	@Resource(name="sqlSessionTemplate_app_server")
	private SqlSession app_server_sql;
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	
	@Override
	public String getAppList(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList  = app_server_sql.selectList(namespace + "getAppList", chartVo);
	    
		List list = new ArrayList<ChartVo>();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			
			map.put("id", dataList.get(i).getId());
			map.put("appId", dataList.get(i).getAppId());
			map.put("name", dataList.get(i).getAppName());
			map.put("url", dataList.get(i).getUrl());
			
			list.add(map);
		}; 
  
		Map dataMap = new HashMap(); 
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String removeApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "removeApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}       

	@Override
	public String addNewApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "addNewApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}
	
	
	
	//common func
	
	
	
	@Override
	public String getFacilitiesStatus(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getFacilitiesStatus", chartVo);

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("cd", dataList.get(i).getCd());
			map.put("name", dataList.get(i).getName());
			map.put("nameAbb", dataList.get(i).getNameAbb());
			map.put("WCCD", dataList.get(i).getWcCd());
			map.put("jig", URLEncoder.encode(dataList.get(i).getJig(), "UTF-8"));
			map.put("isAuto", URLEncoder.encode(dataList.get(i).getIsAuto(), "UTF-8"));
			map.put("isvalue", dataList.get(i).getIsvalue());
			map.put("type", URLEncoder.encode(dataList.get(i).getType(), "UTF-8"));
			map.put("EXCD", dataList.get(i).getEXCD());
			map.put("NC", dataList.get(i).getNC());
			map.put("PRDPRGM", dataList.get(i).getPRDPRGM());
			map.put("CNTMCD", dataList.get(i).getCNTMCD());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("tgRunTime", dataList.get(i).getTgRunTime());
			map.put("odr", dataList.get(i).getOdr());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String updateDvcStatus(String val) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			chartVo.setDvcName(tempObj.get("dvcName").toString());
			chartVo.setDvcJig(tempObj.get("dvcJig").toString());
			chartVo.setOdr(tempObj.get("odr").toString());

			list.add(chartVo);

		}

		String str = "";
		
		try {
			sql.insert(namespace + "updateDvcStatus", list);
			str = "success";
		}catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}
		

		return str;
	}


};