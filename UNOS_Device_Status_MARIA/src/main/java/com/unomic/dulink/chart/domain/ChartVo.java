
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String jig;
	String mcTy;
	String group;
	String ncTy;
	String cd;
	String nameAbb;
	String wcCd;
	String WC;
	String isAuto;
	String isvalue;
	String type;
	String EXCD;
	String NC;
	String PRDPRGM;
	String CNTMCD;
	String tgCnt;
	String tgRunTime;
	String odr;
	
	//common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String dvcName;
	String dvcJig;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
}
